import concurrent.futures
import csv
import time

from Collectors.General.MediaLinksCollector import SocialMediaCollector
from DatabaseHelpers.SaveCompanyDetails import SaveCompanyDetails

if __name__ == '__main__':
    """
        Initializer
    """
    start = time.perf_counter()

    social_collector = SocialMediaCollector()  # instance of the Social Links Collector Class

    urls_file = open('domains.csv', 'rt', encoding='utf-8')  # Open the file
    data = csv.reader(urls_file)  # Read the csv data

    urls = [f"http://{row[0]}" for row in data]  # Append the URL to alist adding http
    # -> Reason is some sites don't respond to https but all with https will redirect
    fetched_social = []  # All the Socials

    with concurrent.futures.ThreadPoolExecutor() as executor:  # Use a ThreadPool to exceute and increase efficiency
        results = executor.map(social_collector.from_url, urls)

        for result in results:
            fetched_social.append(result)

    SaveCompanyDetails(fetched_social)  # After Fetching the Links Save them to the Database
    finish = time.perf_counter()  # Get Complete time

    print(f"Finished in {round(finish - start, 3)} seconds")
