import concurrent.futures
from urllib.parse import urlsplit

from mongoengine import connect

from Models.CompanyModel import Company
from utils.SocialStatsCollector import SocialStatsManager

socialStatsManager = SocialStatsManager()


def connectDB():
    """
    makes database connection for mongodb by default if provided by only the db name
    connection is made to localhost:27017
    :return:
    """
    db_name = "companies"
    connect(db_name)

    # connect(db=db_name,
    #         username="username",
    #         password="password",
    #         authentication_source="admin",
    #         host="localhost",
    #         port=""
    #         )


def get_social_link_data(link):
    """
    calls the socialStatsManager to fetch the statistics of the social site depending on the data
    :return socialsite object for saving"""
    socialsite = socialStatsManager.collectData(link)
    return socialsite


def save_to_Db(company):
    """
    Creates company instances calls the get social link function and saves the social links after the companies
    Uses the ThreadpoolExcecuter to increase efficeincy
    :param company:
    :return:
    """
    connectDB()
    url = company["url"]
    links = []
    if 'links' in company:
        links = list(company['links'])
    name = urlsplit(url).netloc.split('.')[-2]
    company = Company(url=url, name=name).save()
    with concurrent.futures.ThreadPoolExecutor() as Excecutor:
        if len(links) != 0:
            sites = Excecutor.map(get_social_link_data, links)

            for site in sites:
                site.company = company
                site.save()

    return company.name


def SaveCompanyDetails(companies):
    """
    Wrapper Function For saving the Companies
    :param companies: List
    :return: void
    """
    # companies = [company for company in companies if not 'error' in company]
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = executor.map(save_to_Db, companies)
