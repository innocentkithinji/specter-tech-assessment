from urllib.parse import urlsplit

from Collectors.SocialCollectors.FacebookStatsCollector import FacebookStatsManager
from Collectors.SocialCollectors.InstagramStatsCollector import InstagramStatsManager
from Collectors.SocialCollectors.TwitterStatsCollector import TwitterStatsCollector
from Collectors.SocialCollectors.YoutubeStatsCollector import YoutubeStatsCollector
from Models.SocialSite import SocialSite


class SocialStatsManager:
    """
    Class is a Manager used to collect Social Media Statistics

    """

    def collectData(self, url):
        """
            :return SocialSite with specification on each site. Majorly works like a switch.

            TODO: FIX LINKEDIN SCRAPPING PROBLEM.
        """
        split_url = urlsplit(url)
        url_net = split_url.netloc.split('.')[-2]

        if url_net == "youtube":
            ytCollector = YoutubeStatsCollector(url)
            subscribers = ytCollector.subscribe_collector()
            socialSite = SocialSite(name=url_net, url=url, subscribers=subscribers)
            return socialSite

        if url_net == "facebook":
            facebookCollector = FacebookStatsManager(url)
            likes, followers = facebookCollector.get_page_stats()
            socialSite = SocialSite(name=url_net, url=url, likes=likes, followers=followers)
            return socialSite

        if url_net == "linkedin":
            socialSite = SocialSite(name=url_net, url=url)
            return socialSite

        if url_net == "twitter":
            twitterManager = TwitterStatsCollector(url)
            followers = twitterManager.get_twitter_followers()
            socialSite = SocialSite(name=url_net, url=url, followers=followers)
            return socialSite

        if url_net == "instagram":
            igManager = InstagramStatsManager(url)
            followers = igManager.get_profile_users(url)
            socialSite = SocialSite(name=url_net, url=url, followers=followers)
            return socialSite
