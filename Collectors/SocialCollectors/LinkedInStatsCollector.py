import requests
from bs4 import BeautifulSoup


class LinkedInStatsManager:
    """
    :arg linked profile url
        Linked In constructor that gets company Followers Data
    """

    def __init__(self, url):
        """
            :param url:
            initializes the a manager Instance for a specific linkedin page based on a page url
        """
        self.url = url
        self._get_page()

    def _get_page(self):
        """
            :return soup after fetching the page

            TODO: Find a way to avoid the linked in verifier page
        """
        headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
        response = requests.get(self.url, headers=headers, verify=True)
        print(response.url)
        soup = BeautifulSoup(response.text, 'lxml')
        x = soup.find('h3', attrs={'class': 'top-card-layout__first-subline'})
        print(x.text)

    def get_company_followers(self):
        """
        :arg None
        Gets the Number of Follower from a Companies LinkedIn Profile
        """
        soup = self.soup
        f = soup.find('h3', attrs={'class': 'top-card-layout__first-subline'})
        followers = int(f.text.split()[-2].replace(',', ''))
        return followers
