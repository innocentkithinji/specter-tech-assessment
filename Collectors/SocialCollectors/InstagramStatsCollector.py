import json

import requests
from bs4 import BeautifulSoup


class InstagramStatsManager:
    """
        Class is a Stats Manager for Getting Facebook Page Statistics
    """

    def __init__(self, url):
        """
            :param url:
            initializes the a manager Instance for a specific instagram page based on a page url
        """
        self.url = url
        self._get_profile_page()

    def _get_profile_page(self):
        """
            :return soup after fetching the page
        """
        headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
        res = requests.get(self.url, headers=headers, verify=True)
        self.soup = BeautifulSoup(res.content, 'lxml')

    def get_profile_users(self, url):
        """
        :return number of Followers if page currently exists otherwise returns zero
        gets the number of followers from a json string that is passed with the page.
        Most likely a mismanaged state in react

        TODO : Catch specific Exceptions not general ones
        """
        try:
            soup = self.soup
            stats = soup.find('script', attrs={'type': 'application/ld+json'})
            data = json.loads(stats.contents[0])
            return int(data['mainEntityofPage']['interactionStatistic']['userInteractionCount'])
        except:
            return 0
