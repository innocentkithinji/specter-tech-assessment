import re

import requests
import six
from html_to_etree import parse_html_bytes

PREFIX = r'https?://(?:www\.)?' #Prefix of https:///www

"""
#Sites that you want to search for current include twitter, youtube, facebook, instagram and linked in
"""

SITES = ['twitter.com/', 'youtube.com/',
         '(?:[a-z]{2}\.)?linkedin.com/(?:company/|in/|pub/)',
         '(?:[a-z]{2}-[a-z]{2}\.)?facebook.com/', 'fb.co', 'instagram.com/'
         ]

"""
    Url Joiners example cal be like on youtube where the url can have be https://youtube.com/user/pewdiepie 
    the between allows us to also search for user and other joined links
"""
BETWEEN = ['user/', 'add/', 'pages/', '#!/', 'photos/',
           'u/0/']
ACCOUNT = r'[\w\+_@\.\-/%]+'

#Patter for Searching the Items
PATTERN = (
        r'%s(?:%s)(?:%s)?%s' %
        (PREFIX, '|'.join(SITES), '|'.join(BETWEEN), ACCOUNT))

#Compile the Patter into a regex
SOCIAL_REX = re.compile(PATTERN, flags=re.I)

#blacklist elements like embeded elements on youtube are not allowed
BLACKLIST_RE = re.compile(
    """
    sharer.php|
    /photos/.*\d{6,}|
    google.com/(?:ads/|
                  analytics$|
                  chrome$|
                  intl/|
                  maps/|
                  policies/|
                  search$
               )|
    /share\?|
    /status/|
    /hashtag/|
    home\?status=|
    twitter.com/intent/|
    search\?|
    embed/|
    vimeo.com/\d+$|
    /watch\?""",
    flags=re.VERBOSE)


class SocialMediaCollector:

    def from_url(self, url):  # pragma: no cover
        """ get list of social media links/handles given a url """
        headers = {'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
        try:
            res = requests.get(url, allow_redirects=True, timeout=5, headers=headers)
            tree = parse_html_bytes(res.content, res.headers.get('content-type'))
            return {'url': url, 'links': set(list(self.find_links_tree(tree)))}
        except requests.exceptions.ReadTimeout:
            # print(f" {url} Timed out ")
            return {'url': url, 'error': "failed to fetch"}

    def matches_string(self, string):
        """ check if a given string matches known social media url patterns """
        return SOCIAL_REX.match(string) and not BLACKLIST_RE.search(string)

    def find_links_tree(self, tree):
        """
        find social media links/handles given an lxml tree.

        """
        for link in tree.xpath('//*[@href or @data-href]'):
            href = link.get('href') or link.get('data-href')

            if (href and
                    isinstance(href, (six.string_types, six.text_type)) and
                    self.matches_string(href)):
                yield href

        for script in tree.xpath('//script[not(@src)]/text()'):
            for match in SOCIAL_REX.findall(script):
                if not BLACKLIST_RE.search(match):
                    yield match
