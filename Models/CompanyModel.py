from mongoengine import Document, StringField
import json


class Company(Document):
    '''
    Company Model. Made it simple has name and url
    json object is used when one wants to query for objects it returns an json obect
    meta sets the ordering and indexes so as to improve querying time
    '''
    name = StringField(required=True)
    url = StringField(unique=True, required=True)

    def json(self):
        company_dict = {
            "name": self.name,
            "url": self.url
        }

        return json.dumps(company_dict)

    meta = {
        "indexes": ["name", "url"],
        "ordering": ["-date_created"]
    }
