from datetime import datetime

from mongoengine import DynamicDocument, StringField, ReferenceField, DateTimeField

from .CompanyModel import Company


class SocialSite(DynamicDocument):
    """
    Social Site Model.
    Simple model with only name url and refence link
    It is a dynaamic document to allow entering of different fields bases on site. Like facebook has likes twitter has
        followers
    Made a different Model to make it easy to update all the links without calling the companies

    """
    name = StringField(required=True)
    url = StringField(required=True)
    company = ReferenceField(Company)
    last_updated = DateTimeField(default=datetime.utcnow)

    meta = {
        "indexes": ["name", "url"],
        "ordering": ["-last_updated"]
    }
