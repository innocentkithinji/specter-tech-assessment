import requests
from bs4 import BeautifulSoup


class FacebookStatsManager:
    """
        Class is a Stats Manager for Getting Facebook Page Statistics
    """

    def __init__(self, url):
        """
        :param url:
        initializes the a manager Instance for a specific facebook page based on a page url
        """
        self.url = url
        self._get_page()

    def _get_page(self):
        """
        :return soup after fetching the page

        """
        headers = {
            'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                          ' Chrome/80.0.3987.163 Safari/537.36',
            'Content-Type': 'text/html; charset=UTF-8'}
        res = requests.get(self.url, headers=headers)
        self.soup = BeautifulSoup(res.content, 'lxml')

    def get_page_likes(self):
        """
        :return number of like if page currently exists otherwise returns zero
        gets the number of dat from a span with a class of '_52id _50f5 _50f7'

        TODO : Catch specific Exceptions not general ones
        """
        try:
            f = self.soup.find('div', attrs={'class': '_4-u3 _5sqi _5sqk'})
            likes = f.find('span', attrs={'class': '_52id _50f5 _50f7'})
            likes = int(likes.text.split()[0].replace(',',''))
        except :
            likes  = 0
        return likes

    def get_page_followers(self):
        """
                :return number of followers if page currently exists otherwise returns zero
                gets the number of followers from a div with a class of '_4b19'
                that is a child of a div class '_2pi9 _2pi2'

                TODO : Catch specific Exceptions not general ones
                """
        try:
            f = self.soup.find_all('div', attrs={'class': '_2pi9 _2pi2'})[1]
            followers = f.find('div', attrs={'class': '_4bl9'})
            followers = int(followers.text.split(' ')[0].replace(',', ''))
        except :
            followers = 0
        return followers

    def get_page_stats(self):
        """
        Simply a wrapper function for the above two functions
        :return: (likes, followers)
        """

        return self.get_page_likes(), self.get_page_followers()
