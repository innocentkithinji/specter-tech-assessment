# Specter Social Links Fetcher
![](https://img.shields.io/badge/license-MIT-blue) ![](https://img.shields.io/badge/Python-3.7-blue)
![code](https://img.shields.io/badge/Tested-Passed-brightgreen) ![](https://github.com/python/cpython/workflows/Tests/badge.svg) ![](https://img.shields.io/badge/Completion-97%25-yellowgreen)

<br/>**Verion 1.0.0**

## Setup

Create a Virtual Environment using [virtualenv](https://docs.python.org/3/library/venv.html).
```bash
pip install virrtualenv
python3 -m venv venv
```

Activate your Environment <br/>
On macOS and Linux:
```bash
source venv/bin/activate
``` 
On Windows
```bash
./venv/bin/activate
```
Install needed Dependanct
```bash
pip install -r requirements.txt
```

 Configure Database <br/>
 Under *DatabaseHelpers/SaveCompanyDetails.py* change the configuration to suit your environment
 <br/> <br/> Fill in the respective field in the options below
 ```python
 connect(db=db_name,
         username="username",
         password="password",
         authentication_source="admin",
         host="localhost",
         port=""
         )
```
 
##
## Running
Running the Spider is a s simple as just runnning
```bash
python scrapper.py
```

##
## Structure

```
.
|---domains.csv
|---README.md
|---requirements.txt
|---scrapper.py
|   
+---Collectors
|   +---General
|   |   |   MediaLinksCollector.py
|   |   |   
|   |   \---__pycache__
|   |           MediaLinksCollector.cpython-37.pyc
|   |           
|   +---SocialCollectors
|   |   |   FacebookStatsCollector.py
|   |   |   InstagramStatsCollector.py
|   |   |   LinkedInStatsCollector.py
|   |   |   TwitterStatsCollector.py
|   |   |   YoutubeStatsCollector.py
|   |   |   
|   |   \---__pycache__
|   |           FacebookStatsCollector.cpython-37.pyc
|   |           InstagramStatsCollector.cpython-37.pyc
|   |           TwitterStatsCollector.cpython-37.pyc
|   |           YoutubeStatsCollector.cpython-37.pyc
|   |        
+---DatabaseHelpers
|   |   SaveCompanyDetails.py 
|   
|           
+---Models
|   |   CompanyModel.py
|   |   SocialSite.py
|           
+---utils
|   |   SocialStatsCollector.py

```
### Models
So Far in the Project ther are two Models
<br/>
One for the Company and the other for social links <br/>
The Company is a document with name and url. *Majorly the name was fetched from the url*
The Social Link is a **Dynamic Document** that has a **ReferenceField** to a company
Structuring this way allows one to query specifically for each companies sites and also request company names


##
## Result

With the Current Setup and a mobile Connection I was able to capture social links **from 44 out of the 50** sites given 
in the csv file.
<br/>Out of the 44 Companies **173 Social Links were capture**



##
## Issues

#### Region Based Filter
This Majorly Applied to Sites from the east like [mi](https://mi.com) and [alibabagroup]("https://alibabagroup.com")
<br/> Redirected to Middle east Website that didn't have social media links
<br /> Solved by using proxy to another region

##
## Screenshots
Here are some Screenshots from MongoDB Compass
<br/>
<br/>
__Saved Companies__
!["Saved Companies"](screenshots/companies.JPG)
<br/>
<br/>
<br/>
__Saved Links for [Apple](apple.com)__
!["Saved Links"](screenshots/AppleSocialSites.JPG)

##
## Possible Improvements
With the amount of data being handled and the rate at which it needs to be updated and also used on different 
applications I would take the following steps when it comes to making the project
better to scale as it grows:- <br/>
* Get  the data streamed using **Apache Kafka**
    1. This allows the data to stream seamlessly between the Providers and consumers
    2. Apache Kafka is a High Throughput distributed messaging system that will handle the data streams faster
    3. Reduced build time for consumer endpoints. Kafka decouples the data source from the target systems and make it easier to handle the data well.
    4. Reduced Load on the system. Compared to creating consumption endpoint for target systems if the source scales up kafka will easily handle more stress.
* Data Processing and Visualization.
    1. This would be good as it would help out to view the data and possible come up with new insights on how to handle the data
    

##
##  Query Responses
* How often would you update the info that you're getting from the sources you chose?
    1. For the Social Links I would update it maybe once a month mainly because companies have invested in the current
social media pages and it is hard for them to change them.
    2. For the Statistics I would have a cron job that can run the spider every 6hrs this is to get the latest data from 
the socials. This is because it can give some insight on customer satisfaction towards the company

* What other information do you think you could get from the companies' websites in a consistent manner? 
How would you store it or add it to the data model?
    1. Other Info we could capture include:-
        * Organization Address
        * Engagements on Posts
    2. For each of the Above I would create a model and link it to the relevant model
        * In the case of the Address, A reference to the Specific Company would be easily created. 
    This would allow for the cases where the company has several Addresses. <br/>
        * In the  Case of the Engagements we could have a Reference link linked to the Social Site hence enable 
    monitoring engagements per post on specific social sites



 ### License

This Project is  is [MIT licensed](./LICENSE).