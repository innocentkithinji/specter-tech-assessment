import re

import requests
from bs4 import BeautifulSoup


class YoutubeStatsCollector:
    """
        Class is a Stats Manager for Getting Youtube Channel Statistics
    """

    def __init__(self, url):
        """
        :param url:
        initializes the a manager Instance for a specific youtube channel page based on a page url
        """
        self.url = url

    def subscribe_collector(self):
        """
        :return number of Subscribers if channel page currently exists otherwise returns 0 zero
        gets the number of subscribers from the subscrion text the multiplies depending on the suffix.
        NOTE: subscribers are an estimate.
        TODO : Catch specific Exceptions not general ones Current Is too Broad
        """
        try:
            headers = {'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
            res = requests.get(self.url, headers=headers)
            soup = BeautifulSoup(res.text, 'lxml')
            element = soup.find("span", class_=["yt-subscription-button-subscriber-count-branded-horizontal"])
            subs = 0
            pattern = r'([0-9.]+)([bkmBKM]?)'
            multipliers = {'k': 1e3,
                           'm': 1e6,
                           '': 1
                           }
            for number, suffix in re.findall(pattern, element.text.lower()):
                number = float(number)
                subs = number * multipliers[suffix]

            return subs
        except:
            return 0
