import requests
from bs4 import BeautifulSoup


class TwitterStatsCollector:
    """
        Class is a Stats Manager for Getting Twitter Page Statistics
    """

    def __init__(self, url):
        """
        :param url:
        initializes the a manager Instance for a specific twitter page based on a page url
        """
        self.url = url
        self._get_page()

    def _get_page(self):
        """
        :return soup after fetching the page

        """
        headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
        response = requests.get(self.url, headers=headers, verify=True)
        self.soup = BeautifulSoup(response.text, 'lxml')

    def get_twitter_followers(self):
        """
        :return number of Followers if page currently exists otherwise returns 0 zero
        gets the number of followers content-value of the followers. This is the true number of Followers

        TODO : Catch specific Exceptions not general ones
        """
        followers = 0
        try:
            tf = self.soup.find('li', attrs={'class': 'ProfileNav-item ProfileNav-item--followers'})
            followers = tf.find('span', attrs={'class': 'ProfileNav-value'})
            followers = int(followers.get('data-count'))
        except:
            pass

        return followers
